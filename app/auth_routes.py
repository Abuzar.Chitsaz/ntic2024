from flask import Blueprint, render_template, redirect, url_for, request, flash
from flask_login import login_user, logout_user, login_required, current_user
from .forms import LoginForm, RegisterForm
from .models import Users, db
from werkzeug.security import generate_password_hash, check_password_hash

auth_bp = Blueprint('auth', __name__)

@auth_bp.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        email = request.form['email']
        password = request.form['password']

        user = Users.query.filter_by(email=email).first()

        if user and check_password_hash(user.password, password):
            # Si les informations sont correctes, connectez l'utilisateur
            login_user(user)
            return redirect(url_for('project.index'))
        else:
            flash('Invalid email or password.')

    return render_template('login.html')

@auth_bp.route('/register', methods=['GET', 'POST'])
def register():
    form = RegisterForm()
    if form.validate_on_submit():
        username = form.username.data
        email = form.email.data
        password = form.password.data

        # Vérification que l'utilisateur n'existe pas déjà
        user_exists = Users.query.filter_by(email=email).first()
        if user_exists:
            flash('Email already registered.', 'error')
            return redirect(url_for('auth.register', form=form))
        user_exists = Users.query.filter_by(email=email).first()
        

        if len(form.password.data) < 8:
            flash('Password must be at least 8 characters long.', 'error')
            return redirect(url_for('auth.register', form=form))
        
        hashed_password = generate_password_hash(password, method='pbkdf2:sha256')
        new_user = Users(username=username, email=email, password=hashed_password)
        db.session.add(new_user)
        db.session.commit()
        
        flash('You have been registered successfully!', 'success')
        return redirect(url_for('auth.login'))
        print("Form is valid")
    else:
        print("Form is not valid", form.errors)

    return render_template('register.html', form=form)

@auth_bp.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('auth.login'))
