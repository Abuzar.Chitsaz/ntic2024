from flask import Blueprint, render_template, redirect, url_for, request, flash, current_app, send_file, abort
from flask_login import login_required, current_user
from datetime import date
from sqlalchemy.exc import SQLAlchemyError
from werkzeug.utils import secure_filename
from io import BytesIO
from . import db
import imghdr

from .models import Projects, Prototypes, Images, Clients
from .forms import ClientForm, ProjectForm



project_bp = Blueprint('project', __name__)

@project_bp.route('/index')
@login_required
def index():
    return render_template('index.html')


@project_bp.route('/projects')
@login_required
def list_projects():
    projects = Projects.query.filter_by(userID=current_user.userID).all()
    return render_template('project_list.html', projects=projects)

@project_bp.route('/project/<int:project_id>')
@login_required
def view_project(project_id):
    project = Projects.query.get_or_404(project_id)
    prototypes = Prototypes.query.filter_by(projectID=project.projectID).all()
    return render_template('project_detail.html', project=project, prototypes=prototypes)

@project_bp.route('/add_project', methods=['GET', 'POST'])
@login_required
def add_project():
    form = ProjectForm()
    form.client_id.choices = [(c.clientID, c.name) for c in Clients.query.all()]
    if form.validate_on_submit():
        try:
            new_project = Projects(
                name=form.project_name.data,
                userID=current_user.userID,
                clientID=form.client_id.data
            )
            db.session.add(new_project)
            db.session.commit()
            flash('Project added successfully', 'success')
            return redirect(url_for('project.list_projects'))
        except SQLAlchemyError as e:
            db.session.rollback()
            flash(f'Failed to add the project. Error: {e}', 'error')
    return render_template('add_project.html', form=form)

@project_bp.route('/clients')
@login_required
def list_clients():
    clients = Clients.query.all()  # Récupère tous les clients de la base de données
    return render_template('client_list.html', clients=clients)

@project_bp.route('/add_client', methods=['GET', 'POST'])
@login_required
def add_client():
    form = ClientForm()
    if form.validate_on_submit():
        try:
            new_client = Clients(
                name=form.name.data,
                email=form.email.data,
                dateOfBirth=form.dateOfBirth.data
            )
            db.session.add(new_client)
            db.session.commit()
            flash('Client added successfully!', 'success')
            return redirect(url_for('project.list_clients'))
        except SQLAlchemyError as e:
            db.session.rollback()
            flash(f'Failed to add client. Error: {e}', 'error')

    return render_template('add_client.html', form=form)

@project_bp.route('/project/<int:project_id>/add_prototype', methods=['GET', 'POST'])
@login_required
def add_prototype(project_id):
    project = Projects.query.get_or_404(project_id)
    if request.method == 'POST':
        description = request.form.get('description')
        try:
            # Assurez-vous d'utiliser 'projectID' qui est le nom correct de l'attribut dans le modèle
            new_prototype = Prototypes(description=description, projectID=project_id, creationDate=date.today())
            db.session.add(new_prototype)
            db.session.commit()
            flash('Prototype added successfully.')
            return redirect(url_for('project.view_project', project_id=project_id))
        except SQLAlchemyError as e:
            db.session.rollback()
            flash(f'Error adding prototype: {str(e)}', 'error')

    return render_template('add_prototype.html', project=project)

@project_bp.route('/prototype/<int:prototype_id>')
@login_required
def view_prototype(prototype_id):
    prototype = Prototypes.query.get_or_404(prototype_id)
    images = Images.query.filter_by(prototypeID=prototype_id).all()
    return render_template('prototype_detail.html', prototype=prototype, images=images)

@project_bp.route('/prototype/<int:prototype_id>/add_image', methods=['POST'])
@login_required
def add_image_to_prototype(prototype_id):
    file = request.files['image']
    if file and allowed_file(file.filename):
        filename = secure_filename(file.filename)
        try:
            # Lire le contenu du fichier
            image_data = file.read()
            new_image = Images(prototypeID=prototype_id, imageData=image_data)
            db.session.add(new_image)
            db.session.commit()
            flash('Image added successfully.')
        except Exception as e:
            db.session.rollback()
            flash(f'Failed to add image. Error: {e}', 'error')
    else:
        flash('Invalid file or file type.')

    return redirect(url_for('project.view_prototype', prototype_id=prototype_id))

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in current_app.config['ALLOWED_EXTENSIONS']


@project_bp.route('/image/<int:image_id>')
def serve_image(image_id):
    image = Images.query.get(image_id)
    if not image or not image.imageData:
        abort(404)
        
    
    image_type = imghdr.what(None, h=image.imageData)
    if image_type not in current_app.config['ALLOWED_EXTENSIONS']:
        abort(404)  # Si le type n'est pas autorisé ou détecté, retourne une erreur 404
    
    #send_file : Cette fonction de Flask prend en charge l'envoi de fichiers au client. Ici, vous utilisez BytesIO pour créer un fichier en mémoire à partir de votre BLOB, que send_file peut ensuite envoyer comme s'il s'agissait d'un fichier réel.
    return send_file(
        BytesIO(image.imageData),
        mimetype=f'image/{image_type}',  # Utilise le type détecté pour le MIME type
        as_attachment=False,
        #attachment_filename=f'image_{image_id}.{image_type}'
    )

@project_bp.route('/image/<int:image_id>/delete', methods=['POST'])
@login_required
def delete_image(image_id):
    image = Images.query.get_or_404(image_id)
    db.session.delete(image)
    db.session.commit()
    flash('Image deleted successfully.', 'success')
    return redirect(url_for('project.view_prototype', prototype_id=image.prototypeID))

@project_bp.route('/client/<int:client_id>/delete', methods=['POST'])
@login_required
def delete_client(client_id):
    client = Clients.query.get_or_404(client_id)
    projects = Projects.query.filter_by(clientID=client.clientID).all()

    if projects:
        flash('Impossible de supprimer le client. Il existe des projets associés à ce client. Veuillez d’abord supprimer les projets', 'error')
        return redirect(url_for('project.list_clients'))

    try:
        db.session.delete(client)
        db.session.commit()
        flash('Client deleted successfully.', 'success')
    except SQLAlchemyError as e:
        db.session.rollback()
        flash(f'Failed to delete client. Error: {e}', 'error')

    return redirect(url_for('project.list_clients'))




@project_bp.route('/project/<int:project_id>/delete', methods=['POST'])
@login_required
def delete_project(project_id):
    project = Projects.query.get_or_404(project_id)
    db.session.delete(project)
    db.session.commit()
    flash('Project deleted successfully.', 'success')
    return redirect(url_for('project.list_projects'))

@project_bp.route('/prototype/<int:prototype_id>/delete', methods=['POST'])
@login_required
def delete_prototype(prototype_id):
    prototype = Prototypes.query.get_or_404(prototype_id)
    images = Images.query.filter_by(prototypeID=prototype_id).all()

    # Supprimer toutes les images liées
    for image in images:
        db.session.delete(image) 
    # Ensuite, supprimer le prototype
    db.session.delete(prototype)
    try:
        db.session.commit()
        flash('Prototype and all associated images deleted successfully.', 'success')
    except SQLAlchemyError as e:
        db.session.rollback()
        flash(f'Failed to delete prototype. Error: {str(e)}', 'error')
    return redirect(url_for('project.view_project', project_id=prototype.projectID))


@project_bp.route('/project/<int:project_id>/edit', methods=['GET', 'POST'])
@login_required
def edit_project(project_id):
    project = Projects.query.get_or_404(project_id)
    form = ProjectForm(obj=project)  # Initialise le formulaire avec l'objet project
    form.client_id.choices = [(c.clientID, c.name) for c in Clients.query.all()]  # Mettre à jour les choix de clients

    if form.validate_on_submit():
        project.name = form.project_name.data
        project.clientID = form.client_id.data
        db.session.commit()
        flash('Project updated successfully.', 'success')
        return redirect(url_for('project.list_projects'))
    
    form.client_id.data = project.clientID
    return render_template('edit_project.html', form=form, project=project)

@project_bp.route('/prototype/<int:prototype_id>/edit', methods=['GET', 'POST'])
@login_required
def edit_prototype(prototype_id):
    prototype = Prototypes.query.get_or_404(prototype_id)
    if request.method == 'POST':
        description = request.form.get('description')
        if description:
            prototype.description = description
            db.session.commit()
            flash('Prototype updated successfully.', 'success')
            return redirect(url_for('project.view_prototype', prototype_id=prototype_id))
        else:
            flash('Description cannot be empty.', 'error')

    return render_template('edit_prototype.html', prototype=prototype)
    


