from flask_sqlalchemy import SQLAlchemy
from flask_login import UserMixin, LoginManager
from sqlalchemy.orm import relationship

db = SQLAlchemy()
login_manager = LoginManager()

class Users(db.Model, UserMixin):
    userID = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(255), nullable=False, unique=True)
    email = db.Column(db.String(255), unique=True, nullable=False)
    password = db.Column(db.String(255), nullable=False)
    projects = db.relationship('Projects', backref='owner', lazy=True)

    def get_id(self):
        """Retourne l'identifiant de l'utilisateur sous forme de chaîne pour compatibilité avec Flask-Login."""
        return str(self.userID)

class Clients(db.Model):
    clientID = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), nullable=False)
    dateOfBirth = db.Column(db.Date, nullable=False)
    email = db.Column(db.String(255), unique=True, nullable=False)
    projects = db.relationship('Projects', backref='clients', lazy=True)

class Projects(db.Model):
    __tablename__ = 'projects'
    projectID = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable=False)
    userID = db.Column(db.Integer, db.ForeignKey('users.userID'), nullable=False)
    clientID = db.Column(db.Integer, db.ForeignKey('clients.clientID'), nullable=False)
    prototypes = relationship('Prototypes', backref='project', cascade="all, delete-orphan")

class Prototypes(db.Model):
    __tablename__ = 'prototypes'
    prototypeID = db.Column(db.Integer, primary_key=True)
    description = db.Column(db.String(200), nullable=False)
    projectID = db.Column(db.Integer, db.ForeignKey('projects.projectID'), nullable=False)
    creationDate = db.Column(db.Date, nullable=False)
    images = relationship('Images', backref='prototype', cascade="all, delete-orphan")

class Images(db.Model):
    __tablename__ = 'images'
    imageID = db.Column(db.Integer, primary_key=True)
    imageData = db.Column(db.LargeBinary, nullable=False) # Utilisation de LargeBinary pour les données BLOB
    prototypeID = db.Column(db.Integer, db.ForeignKey('prototypes.prototypeID'), nullable=False)