@startuml
package "Flask Application" {
    package "Models" {
        class Users {
            - int userID
            - string username
            - string email
            - string password
            + check_password(password)
        }

        class Clients {
            - int clientID
            - string name
            - string email
            - date dateOfBirth
        }

        class Projects {
            - int projectID
            - string name
            - int userID
            - int clientID
        }

        class Prototypes {
            - int prototypeID
            - string description
            - date creationDate
            - int projectID
        }

        class Images {
            - int imageID
            - binary imageData
            - int prototypeID
        }

        Users "1" --> "0..*" Projects : "manages"
        Clients "1" --> "0..*" Projects : "owns"
        Projects "1" --> "0..*" Prototypes : "contains"
        Prototypes "1" --> "0..*" Images : "includes"
    }

    package "Controllers" {
        class AuthController {
            + login()
            + register()
            + logout()
        }

        class ProjectController {
            + index()
            + list_projects()
            + view_project(project_id)
            + add_project()
            + delete_project(project_id)
            + edit_project(project_id)
            + add_prototype(project_id)
            + view_prototype(prototype_id)
            + add_image_to_prototype(prototype_id)
            + delete_image(image_id)
            + edit_prototype(prototype_id)
        }

        class ClientController {
            + list_clients()
            + add_client()
            + delete_client(client_id)
        }

        AuthController --> Users : "manages"
        ProjectController --> Projects : "manages"
        ProjectController --> Prototypes : "manages"
        ProjectController --> Images : "manages"
        ClientController --> Clients : "manages"
    }

    package "Views" {
        class LoginView {
            + render_login_form()
        }

        class RegisterView {
            + render_register_form()
        }

        class ProjectListView {
            + render_project_list()
        }

        class ProjectDetailView {
            + render_project_detail()
        }

        class PrototypeDetailView {
            + render_prototype_detail()
        }

        class ClientListView {
            + render_client_list()
        }

        LoginView --> AuthController : "interacts"
        RegisterView --> AuthController : "interacts"
        ProjectListView --> ProjectController : "interacts"
        ProjectDetailView --> ProjectController : "interacts"
        PrototypeDetailView --> ProjectController : "interacts"
        ClientListView --> ClientController : "interacts"
    }
}

@enduml