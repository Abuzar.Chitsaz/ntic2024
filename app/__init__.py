from flask import Flask, redirect, url_for, render_template
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from datetime import date
from .models import db, login_manager, Users
from config import Config

login_manager.login_view = 'auth.login'  # Spécifie la route de login pour les utilisateurs non authentifiés

def create_app():
    
    app = Flask(__name__)
    app.config.from_object(Config)  # Charge la configuration à partir de l'objet Config
    app.config['WTF_CSRF_ENABLED'] = False #Désactivé pour développement à Activer en cas de vrai déploiement 
    
    # Initialisation des extensions avec l'app Flask
    db.init_app(app)
    login_manager.init_app(app)
    login_manager.login_view = 'auth.login'

    def calculate_age(born):
        today = date.today()
        return today.year - born.year - ((today.month, today.day) < (born.month, born.day))
    
    app.jinja_env.filters['calculate_age'] = calculate_age
    
    @app.route('/')
    def home():
        # Rediriger vers la page de connexion
        return redirect(url_for('auth.login'))

    # Import et enregistrement des blueprints
    from .auth_routes import auth_bp
    from .project_routes import project_bp
    app.register_blueprint(auth_bp, url_prefix='/auth')
    app.register_blueprint(project_bp, url_prefix='/project')
    
    # Importation des modèles et création des tables si elles n'existent pas
    with app.app_context():
        from .models import Users, Clients, Projects, Prototypes, Images
        db.create_all()

        # Fonction de rappel pour charger l'utilisateur connecté et le gérer avec Flask-Login
        @login_manager.user_loader
        def load_user(userID):
            return Users.query.get(int(userID))
    
    return app

