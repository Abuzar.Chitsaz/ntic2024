from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SelectField, SubmitField, DateField
from wtforms.validators import InputRequired, DataRequired, Email, Length

class LoginForm(FlaskForm):
    email = StringField('Email', validators=[InputRequired(), Email(message='Invalid email'), Length(max=50)])
    password = PasswordField('Password', validators=[InputRequired(), Length(min=8, max=80)])
    submit = SubmitField('Login')

class RegisterForm(FlaskForm):
    email = StringField('Email', validators=[InputRequired(), Email(message='Invalid email'), Length(max=50)])
    username = StringField('Username', validators=[InputRequired(), Length(min=4, max=15)])
    password = PasswordField('Password', validators=[InputRequired(), Length(min=8, max=80)])
    submit = SubmitField('Register')

    class Meta:
        csrf = False  # Temporarily disable CSRF for this form only

class ProjectForm(FlaskForm):
    project_name = StringField('Project Name', validators=[DataRequired()])
    client_id = SelectField('Select Client', coerce=int, validators=[DataRequired()])# 'SelectField' avec 'coerce=int' pour s'assurer que l'identifiant du client est transmis comme un entier
    submit = SubmitField('Add Project')
    

class ClientForm(FlaskForm):
    name = StringField('Name', validators=[DataRequired()])
    email = StringField('Email', validators=[DataRequired(), Email()])
    dateOfBirth = DateField('Date of Birth', format='%Y-%m-%d', validators=[DataRequired()])
    submit = SubmitField('Add Client')
