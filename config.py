import os

class Config:
    BASE_DIR = os.path.abspath(os.path.dirname(__file__))  # Chemin absolu du dossier où se trouve le fichier de configuration
    UPLOAD_FOLDER = os.path.join(BASE_DIR, 'uploads')
    ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg', 'gif'}
    SECRET_KEY = os.urandom(24)
    SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://root:@localhost/ntic_db'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
